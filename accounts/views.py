from accounts.forms import SignUpForm
from django.contrib.auth import logout
from django.shortcuts import render, redirect
from accounts.models import LoginForm
from django.contrib.auth import authenticate, login
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")

            form.add_error(None, "The username or password is invalid")

    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": projects}

    return render(
        request, "projects/list_projects.html", {"projects": projects}
    )


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                # Check if username is unique
                if not User.objects.filter(username=username).exists():
                    # Create a new user
                    user = User.objects.create_user(
                        username=username, password=password
                    )
                    login(request, user)  # Log the user in
                    return redirect(
                        "list_projects"
                    )  # Redirect to the list of projects
                else:
                    form.add_error(
                        "username",
                        "Username already exists. Please choose a different username.",
                    )
            else:
                form.add_error(None, "The passwords do not match.")

    else:
        form = SignUpForm()

    return render(request, "accounts/signup.html", {"form": form})
