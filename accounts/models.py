from django.db import models
from django import forms

# Create your models here.


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150, label="Username")
    password = forms.CharField(
        max_length=150, widget=forms.PasswordInput(), label="password"
    )  # widget hides the characters when user adds input
