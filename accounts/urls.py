# urls.py
from django.urls import path
from django.contrib.auth.views import LoginView
from accounts.views import login_view  # Import your project_list view
from accounts.views import logout_view
from accounts.views import signup_view

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", signup_view, name="signup"),
]
