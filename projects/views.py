from django.shortcuts import get_object_or_404, render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from django.contrib.auth.models import User


def list_projects(request):
    projects = Project.objects.all()
    context = {"projects": projects}
    return render(request, "lists/list_projects.html", context)


# Create your views here.
@login_required
def project_detail(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    return render(request, "lists/project_details.html", {"project": project})


@login_required
def create_project(request):
    users = User.objects.all()  # Add this line to get all users
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(
        request, "lists/create_project.html", {"form": form, "users": users}
    )
