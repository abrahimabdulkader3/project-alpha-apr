from django.urls import path
from projects.views import list_projects
from projects.views import project_detail
from projects.views import create_project


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:project_id>/", project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
