# from django.shortcuts import render, redirect, get_object_or_404
# from django.contrib.auth.decorators import login_required
# from projects.views import list_projects
# from tasks.forms import TaskForm
# from tasks.models import Task


# @login_required
# def create_task(request):
#     if request.method == "POST":
#         form = TaskForm(request.POST)
#         if form.is_valid():
#             task = form.save()  # Save the task without assigning the owner
#             return redirect(
#                 "list_projects"
#             )  # Redirect to the list of projects
#     else:
#         form = TaskForm()

#     return render(request, "tasks/create_task.html", {"form": form})
